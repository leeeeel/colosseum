#pragma once

#include "sve_camera.hpp"

// lib
#include <vulkan/vulkan.h>

namespace sve
{
    struct FrameInfo
    {
        int frameIndex;
        float frameTime;
        VkCommandBuffer commandBuffer;
        SveCamera &camera;
    };
} // namespace sve
