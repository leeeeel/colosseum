#pragma once

#include "sve_device.hpp"
#include "sve_swap_chain.hpp"
#include "sve_window.hpp"

// std
#include <cassert>
#include <memory>
#include <vector>

namespace sve
{
    class SveRenderer
    {
    public:
        SveRenderer(SveWindow &window, SveDevice &device);
        ~SveRenderer();

        SveRenderer(const SveRenderer &) = delete;
        SveRenderer &operator=(const SveRenderer &) = delete;

        const VkRenderPass getSwapChainRenderPass() { return sveSwapChain->getRenderPass(); }
        float getAspectRatio() const { return sveSwapChain->extentAspectRatio(); }
        const bool isFrameInProgress() { return isFrameStarted; }
        const VkCommandBuffer getCurrentCommandBuffer()
        {
            assert(isFrameStarted && "Cannot get command buffer when frame not in progress");
            return commandBuffers[currentFrameIndex];
        }

        const int getFrameIndex()
        {
            assert(isFrameStarted && "Cannot get frame index when frame not in progress");
            return currentFrameIndex;
        }

        VkCommandBuffer beginFrame();
        void endFrame();
        void beginSwapChainRenderPass(VkCommandBuffer commandBuffer);
        void endSwapChainRenderPass(VkCommandBuffer commandBuffer);

    private:
        void createCommandBuffers();
        void freeCommandBuffers();
        void recreateSwapChain();

        SveWindow &sveWindow;
        SveDevice &sveDevice;
        std::unique_ptr<SveSwapChain> sveSwapChain;
        std::vector<VkCommandBuffer> commandBuffers;

        uint32_t currentImageIndex;
        int currentFrameIndex{0};
        bool isFrameStarted{false};
    };
} // namespace sve