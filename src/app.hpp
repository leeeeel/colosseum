#pragma once

#include "sve_device.hpp"
#include "sve_game_object.hpp"
#include "sve_renderer.hpp"
#include "sve_window.hpp"

// std
#include <memory>
#include <vector>

namespace sve
{
  class App
  {
  public:
    static constexpr int WIDTH = 800;
    static constexpr int HEIGHT = 600;

    App();
    ~App();

    App(const App &) = delete;
    App &operator=(const App &) = delete;

    void run();

  private:
    void loadGameObjects();

    SveWindow sveWindow{WIDTH, HEIGHT, "simple-vulkan-engine"};
    SveDevice sveDevice{sveWindow};
    SveRenderer sveRenderer{sveWindow, sveDevice};
    std::vector<SveGameObject> gameObjects;
  };
} // namespace sve