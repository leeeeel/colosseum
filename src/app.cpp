#include "app.hpp"

#include "sve_camera.hpp"
#include "simple_render_system.hpp"
#include "keyboard_movement_controller.hpp"

// libs
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

// std
#include <array>
#include <chrono>
#include <stdexcept>

namespace sve
{
  struct GlobalUbo
  {
    glm::mat4 projectionView{1.f};
    glm::vec3 lightDirection = glm::normalize(glm::vec3{1.f, -3.f, -1.f});
  };

  App::App()
  {
    loadGameObjects();
  }

  App::~App() {}

  void App::run()
  {
    SveBuffer globalUboBuffer{
        sveDevice,
        sizeof(GlobalUbo),
        SveSwapChain::MAX_FRAMES_IN_FLIGHT,
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
        sveDevice.properties.limits.minUniformBufferOffsetAlignment,
    };
    globalUboBuffer.map();

    SimpleRenderSystem simpleRenderSystem{sveDevice, sveRenderer.getSwapChainRenderPass()};
    SveCamera camera{};
    camera.setViewTarget(glm::vec3(-1.f, -2.f, 20.f), glm::vec3(0.f, 0.f, 2.5f));

    auto cameraObject = SveGameObject::createGameObject();
    KeyboardMovementController cameraController{};

    auto currentTime = std::chrono::high_resolution_clock::now();

    while (!sveWindow.shouldClose())
    {
      glfwPollEvents();

      auto newTime = std::chrono::high_resolution_clock::now();
      float frameTime = std::chrono::duration<float, std::chrono::seconds::period>(newTime - currentTime).count();
      currentTime = newTime;

      cameraController.moveInPlaneXZ(sveWindow.getGLFWWindow(), frameTime, cameraObject);
      camera.setViewYXZ(cameraObject.transform.translation, cameraObject.transform.rotation);

      float aspect = sveRenderer.getAspectRatio();
      camera.setPerspectiveProjection(glm::radians(50.f), aspect, 0.1f, 100.f);

      if (auto commandBuffer = sveRenderer.beginFrame())
      {
        int frameIndex = sveRenderer.getFrameIndex();
        FrameInfo frameInfo{
            frameIndex,
            frameTime,
            commandBuffer,
            camera};

        //update
        GlobalUbo ubo{};
        ubo.projectionView = camera.getProjection() * camera.getView();
        globalUboBuffer.writeToIndex(&ubo, frameIndex);
        globalUboBuffer.flushIndex(frameIndex);

        //render
        sveRenderer.beginSwapChainRenderPass(commandBuffer);
        simpleRenderSystem.renderGameObjects(frameInfo, gameObjects);
        sveRenderer.endSwapChainRenderPass(commandBuffer);
        sveRenderer.endFrame();
      }
    }

    vkDeviceWaitIdle(sveDevice.device());
  }

  void App::loadGameObjects()
  {
    std::shared_ptr<SveModel> sveModel = SveModel::createModelFromFile(sveDevice, "models/flat_vase.obj");
    auto gameObj = SveGameObject::createGameObject();
    gameObj.model = sveModel;
    gameObj.transform.translation = {.0f, .5f, 2.5f};
    gameObj.transform.scale = glm::vec3{3.f};
    gameObjects.push_back(std::move(gameObj));
  }
} // namespace sve