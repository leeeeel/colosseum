include .env

CFLAGS = -std=c++17 -I. -I$(TINYOBJ_PATH)
LDFLAGS = -lglfw -lvulkan -ldl -lpthread -lX11 -lXxf86vm -lXrandr -lXi 

# create list of all spv files and set as dependency
vertSources = $(shell find ./src/shaders -type f -name "*.vert")
vertObjFiles = $(patsubst %.vert, %.vert.spv, $(vertSources))
fragSources = $(shell find ./src/shaders -type f -name "*.frag")
fragObjFiles = $(patsubst %.frag, %.frag.spv, $(fragSources))

TARGET = a.out
$(TARGET): $(vertObjFiles) $(fragObjFiles)
$(TARGET): src/*.cpp src/*.hpp
	g++ $(CFLAGS) -o $(TARGET) src/*.cpp $(LDFLAGS)

# make shader targets
%.spv: %
	$(GLSLC) $< -o $@

.PHONY: test clean

test: a.out
	./a.out

clean:
	rm -f a.out
	rm -f *.spv


